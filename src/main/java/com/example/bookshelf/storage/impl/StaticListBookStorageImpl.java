package com.example.bookshelf.storage.impl;

import com.example.bookshelf.storage.BookStorage;
import com.example.bookshelf.type.Book;

import java.util.ArrayList;
import java.util.List;

public class StaticListBookStorageImpl implements BookStorage {

    private static List<Book> bookStorage = new ArrayList<Book>();

    public StaticListBookStorageImpl() {
        bookStorage.add(new Book(10, "Harry Potter","J.K", 500,2002,"publishing company ###"));
        bookStorage.add(new Book(12, "Pan Tadeusz","A.M.", 500,1910,"publishing company ###"));
    }

    @Override
    public Book getBook(long id) {
        for (Book book : bookStorage) {
            if (book.getId() == id) {
                return book;
            }
        }
        return null;
    }

    @Override
    public List<Book> getAllBooks() {
        System.out.println();
        return bookStorage;
    }

    @Override
    public void addBook(Book book) {
        bookStorage.add(book);
    }

    @Override
    public void removeBook(long bookIdToDelete) {
        for (Book book : bookStorage) {
            if (book.getId() == bookIdToDelete) {
                bookStorage.remove(book);
                break;
            }
        }
    }
}
